
#include <bits/stdc++.h>
using namespace std;

int knapsack01(int W, const vector<int>& wt, const vector<int>& v) {
  const int N = wt.size();
  vector<vector<int>> dp(N + 1, vector<int>(W + 1, 0));

  for (size_t i = 1; i <= N; i++) {
    for (size_t w = 1; w <= W; w++) {
      const int itemVal = v[i - 1];
      const int itemWt = wt[i - 1];
      if (itemWt > w) {
        dp[i][w] = dp[i - 1][w];
      } else {
        dp[i][w] = max(dp[i - 1][w - itemWt], dp[i - 1][w]);
      }
    }
  }

  return dp[N][W];
}

void solve() {
  int n;
  cin >> n;

  int totalValue = 0;
  for (int i = 0; i < n; i++) {
    int user, capacity;
    cin >> user >> capacity;
    vector<int> priority(n), ram(n);
    for (auto& p : priority) cin >> p;
    for (auto& r : ram) cin >> r;

    int serverValue = knapsack01(capacity, ram, priority);
    cout << "Server " << (i + 1) << "value: " << serverValue << endl;
  }
  cout << "Total Value: " << totalValue << endl;
}

signed main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  solve();

  return 0;
}
main.cpp
Displaying main.cpp.